/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebas;

import com.clases.Teclado;
import java.util.Scanner;
/**
 *
 * @author lupis
 */
public class TestTeclado {
    
  public static void main(String... arg){
        Teclado teclado = null;
        Scanner sc = new Scanner(System.in);
        String numerodeserie;
        String modelo;
        String marca;
        String color;
        double tamaño;
        String op="";
        System.out.println("CAPTURA DE DATOS DE UN TECLADO");
        while(true){
           System.out.print("Dame el numero de serie: ");
           numerodeserie = sc.nextLine();
           System.out.print("Dame la marca del teclado: ");
           marca = sc.nextLine();
           System.out.print("Dame el modelo del teclado: ");
           modelo = sc.nextLine();
           System.out.print("Dame el color del teclado: ");
           color = sc.nextLine();
           System.out.print("Dame el tamaño del teclado: ");
           tamaño = Double.parseDouble(sc.nextLine().trim());
           teclado = new Teclado(numerodeserie, marca,modelo, color,tamaño);
           System.out.println("Los datos que diste son:");
           System.out.println(teclado.toString());
           System.out.println("Para salir teclea <salir>, da <enter> para continuar...");
           op = sc.nextLine().trim().toLowerCase();
           if(op.equals("salir"))
               break;
        }
    }
}

